# MailingServer

A container that allows sending emails via a simple HTTP API.

## Setup

First, create a new `.env` file and fill in the values:

```shell
cp .env.example .env
$EDITOR ./.env
```

Then install the Composer dependencies:

```shell
pushd data/html
composer install
popd
```

Next, build and start the Docker container:

```shell
docker compose up -d --build
```

The API should now be reachable on the current system on port 10176 (or the port you specified in `.env`):

```shell
source ./.env
curl "http://localhost:$PORT/ping.php" # should return "PONG!"
```

### Changing settings

If you changed the `.env` file, it is best to fully recreate the container image from scratch. For this use

```shell
docker compose up -d --build --force-recreate
```

## Creating a template

There is already a sample template provided, which demonstrates all the capabilities. Templates follow the following
structure:

### Template properties

**Type:** 📄 *file*  
**Path:** `properties.json`  
**Required:** *yes*

This file contains the most important properties of the email template. The following fields are available (and all
are required):

```json
{
  "sender": {
    "name": "<The full name of the sender>",
    "address": "<The email address of the sender>"
  },
  "subject": "<The subject line (supports Twig expressions, more below)>"
}
```

### Plain text template

**Type:** 📄 *file*  
**Path:** `body.txt.twig`  
**Required:** *no*

Holds the plain text content of the email body. Supports Twig expressions (more below).

### HTML template

**Type:** 📄 *file*  
**Path:** `body.html.twig`  
**Required:** *no*

Holds the HTML content of the email body. Supports Twig expressions (more below).

### Inline images folder

**Type:** 📂 *folder*  
**Path:** `inline/`  
**Required:** *no*

This folder contains all images that will be available for inlining. All files in this folder will always be attached
to the email, so use it sparingly (&rArr; filesize)!. Reference the images within your HTML template with the `cid:`
URI scheme, e.g.:

```html
<img src="cid:IMG_0038.jpg" alt="My cool photo" />
```

The CID of each image will be the filename on disk inside the `inline/` folder.

### Appendix: Twig templates

Several aspects of this application support Twig templates, as mentioned above. To learn more about the Twig templating
language, visit [twig.symfony.com](https://twig.symfony.com/doc/3.x).

The following variables will be available *in addition to the ones you provide in your request (see below)*:

| Property | Description                        | Value       |
|----------|------------------------------------|-------------|
| `_meta`  | Holds additional, dynamic metadata | *See below* |

The `_meta` variable has the following structure:

| Property             | Description                                                       | Value                             |
|----------------------|-------------------------------------------------------------------|-----------------------------------|
| `template`           | Holds the name of the template.                                   | e.g. `sample`                     |
| `templateProperties` | Holds the template properties.                                    | The contents of `properties.json` |
| `recipient`          | Holds the recipient information.                                  | *See below*                       |
| `recipientName`      | Holds the recipient's name, or the full recipient field.          | e.g. `John Doe`                   |
| `recipientAddress`   | Holds the recipient's email address, or the full recipient field. | e.g. `john.doe@example.com`       |

The `_meta.recipient` variable has the following structure:

| Property  | Description                        | Value                       |
|-----------|------------------------------------|-----------------------------|
| `name`    | Hold the recipient's name          | e.g. `John Doe`             |
| `address` | Hold the recipient's email address | e.g. `john.doe@example.com` |

## Sending an email

Just send a `POST` request to `/`:

```http
POST /
Authorization: Bearer <Your auth token (if enabled)>
Content-Type: application/x-www-form-urlencoded

to%5Bname%5D=John%20Doe&to%5Baddress%5D=john.doe%40example.com&template=sample&variables=%7B%22regNum%22%3A%221234-5678%22%7D
```

Okay, so let's break this down:

First, we sent an `Authorization` header. This is only required if `AUTHORIZATION_TOKENS` was set in the `.env` file.
Otherwise, you can omit this header.

Second, we sent the `Content-Type` header. The format of the request can either be `application/x-www-form-urlencoded`
or `multipart/form-data` if sending attachments (more later).

The following fields can be sent in your request:

| Field         | Required? | Type                                         | Description                                              |
|---------------|-----------|----------------------------------------------|----------------------------------------------------------|
| `to`          | *yes*     | `string\|["name"=>string,"address"=>string]` | The recipient information                                |
| `template`    | *yes*     | `string`                                     | The name of the template                                 |
| `variables`   | *no*      | `string\|object`                             | The variables that will get passes to each Twig template |
| `attachments` | *no*      | `(string\|object)[]`                         | Attachments that get added via URL (more below)          |

Objects can either be passed in the form:

```text
variables[foo][bar]=value&variables[foo][foo]=value&variables[arr][]=item&variables[arr][]=item2
```

or as a URL-encoded JSON payload:

```text
variables=%7B%22foo%22%3A%7B%22bar%22%3A%22value%22%2C%22foo%22%3A%22value%22%7D%2C%22arr%22%3A%5B%22item1%22%2C%22item2%22%5D%7D
```

(the decoded form looks like this:)

```json
{"foo":{"bar":"value","foo":"value"},"arr":["item1","item2"]}
```

### Adding attachments

There are two ways to add attachments: A direct file upload and from a given URL.

> ⚠️ **Please note that large attachments may not be sent by your SMTP server or will be rejected by the receiving mail server.**

#### Direct file upload

To add attachments, simply upload them as files with your multipart request. The file name will be applied to the
attachment. So a part like this:

```http
# ...
--myboundary
Content-Type: text/plain; charset=UTF-8
Content-Disposition: form-data; name="somekey"; filename="hello.txt"

Hello, world!
# ...
```

will show up as the attachment "hello.txt". Note that the `name=` value doesn't matter, as long as it's

1. a valid form field name and
2. doesn't clash with any of the predefined field names above.

#### Via URL

You can also add attachments from a URL. To do this, use the `attachments` field in your request.

It is enough to simply provide a URL for a given attachment. In that case, the MIME-type and filename are, if possible,
extracted from the `Content-Type` and `Content-Disposition` response headers respectively.  
However you might want to pass a filename or MIME-type explicitly. In that case, instead of passing a string, send an
object with the following fields:

| Field      | Required? | Type     | Description                                                                              |
|------------|-----------|----------|------------------------------------------------------------------------------------------|
| `url`      | *yes*     | `string` | The source URL of the attachment file.                                                   |
| `mimeType` | *no*      | `string` | The MIME-type of the attachment. If omitted, tries to read it from the response headers. |
| `filename` | *no*      | `string` | The filename of the attachment. If omitted, tries to read it from the response headers.  |

So the `attachments` field request with several attachments might look something like this:

- `attachments[0]` = `http://example.org/myfile.ext`
- `attachments[1][url]` = `http://example.org/mysecondfile.ext`
- `attachments[2][url]` = `http://example.org/mythirdfile.ext`
- `attachments[2][filename]` = `myfilename.ext`
- `attachments[2][mimeType]` = `text/plain`

Like the `variables` field, you can also pass a JSON string to `attachments` representing the same data.
