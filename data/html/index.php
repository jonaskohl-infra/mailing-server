<?php

use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\Mailer;
use Symfony\Component\Mailer\Transport;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;
use Symfony\Component\Mime\Part\DataPart;
use Twig\Environment;
use Twig\Loader\ArrayLoader;
use Twig\Loader\FilesystemLoader;

header_remove("X-Powered-By");

// keep track of all temp files we created and delete them on shutdown
$tempFiles = [];
register_shutdown_function(function() use ($tempFiles) {
    foreach ($tempFiles as $tempFile)
        unlink($tempFile);
});

function result(array $data, int $responseCode = 200): never {
    header("Content-Type: application/json; charset=UTF-8");
    http_response_code($responseCode);
    echo json_encode($data);
    exit;
}

function error(string $message): array {
    return [
        "success" => false,
        "message" => $message,
    ];
}

function success(string $message = null): array {
    return [
        "success" => true,
        "message" => $message,
    ];
}
function data(array $data = [], string $message = null, bool $success = true): array {
    return [
        "success" => $success,
        "message" => $message,
        "data" => $data,
    ];
}

const TEMPLATE_DIRECTORY = __DIR__ . "/templates";
const ALGO = "aes-256-xts";

$mode = ($_GET["m"] ?? null);
$output_rendered_html = $mode === "d";
$render_properties_encrypted = ($_GET["r"] ?? null);
$render_properties_salt = ($_GET["s"] ?? null);
$render_properties_iv = ($_GET["v"] ?? null);
$render_properties = null;
$build_render_props = $mode === "b";

if ($output_rendered_html && ($render_properties_encrypted === null || $render_properties_salt === null || $render_properties_iv === null)) {
    result(error("No render properties provided"), 400);
}

if ($output_rendered_html) {
    $pass = $_ENV["SECRET"] . "salt=$render_properties_salt";
    $render_properties = openssl_decrypt($render_properties_encrypted, ALGO, $pass, iv: base64_decode($render_properties_iv));
    if ($render_properties === false || !json_validate($render_properties)) {
        result(error("Invalid render properties"), 400);
    }
}

if ($render_properties === null) {
    if ($_SERVER["REQUEST_METHOD"] !== "POST") {
        header("Content-Type: text/plain");
        http_response_code(405);
        echo "Cannot $_SERVER[REQUEST_METHOD] /\n";
        exit;
    }

    $authTokens = $_ENV["AUTHORIZATION_TOKENS"] ?? null;
    if ($authTokens !== null) $authTokens = explode(":", $authTokens);
    if (count($authTokens) < 1) $authTokens = null;
    $requiresAuthorization = $authTokens !== null;

    $userToken = $_SERVER["HTTP_AUTHORIZATION"] ?? null;
    if ($userToken !== null) {
        if (!str_starts_with($userToken, "Bearer "))
            result(error("Invalid format for header \"Authorization\""), 400);
        $userToken = substr($userToken, strlen("Bearer "));
    }

    if ($requiresAuthorization && ($userToken === null || !in_array($userToken, $authTokens))) {
        result(error("Invalid access token"), 401);
    }

    if (($_SERVER["HTTP_CONTENT_TYPE"] ?? null) === "application/json") {
        $_POST = json_decode(file_get_contents("php://input"), true);
    }
} else {
    $_POST = json_decode($render_properties, true);
}

$templateName = $_POST["template"] ?? null;
$variables = $_POST["variables"] ?? [];
$recipient = $_POST["to"] ?? [];
$urlAttachments = $_POST["attachments"] ?? [];

if (is_null($templateName) || !preg_match('/^[a-z0-9-_]+$/', $templateName)) {
    result(error("Invalid template name " . json_encode($templateName)), 400);
}

if (is_array($recipient) && !isset($recipient["name"]) && !isset($recipient["address"])) {
    result(error("Malformed field \"to\""), 400);
}

if (!is_array($urlAttachments) && json_validate($urlAttachments)) {
    $urlAttachments = json_decode($urlAttachments, true);
}
if (!is_array($urlAttachments)) {
    result(error("Malformed field \"attachments\""), 400);
}

$templateDir = TEMPLATE_DIRECTORY . "/$templateName";
$htmlTemplateFile = "$templateDir/body.html.twig";
$plaintextTemplateFile = "$templateDir/body.txt.twig";
$propertiesFile = "$templateDir/properties.json";
$mediaDir = "$templateDir/inline";

if ((!is_file($htmlTemplateFile) && !is_file($plaintextTemplateFile)) || !is_file($propertiesFile)) {
    result(error("Unknown or incomplete template \"$templateName\""), 400);
}

if (is_string($variables) && json_validate($variables)) {
    $variables = json_decode($variables, true);
} elseif (!is_array($variables)) {
    result(error("Malformed field \"variables\""), 400);
}

$templateProperties = json_decode(file_get_contents($propertiesFile));

// attach meta variables
$variables["_meta"] = [
    "template" => $templateName,
    "templateProperties" => $templateProperties,
    "recipient" => $recipient,
    "recipientName" => $recipient["name"] ?? $recipient,
    "recipientAddress" => $recipient["address"] ?? $recipient,
];

require_once __DIR__ . "/vendor/autoload.php";

$rp_renderProps = json_encode($_POST, JSON_UNESCAPED_SLASHES);
$rp_salt = base64_encode(random_bytes(32));
$rp_pass = $_ENV["SECRET"] . "salt=$rp_salt";
$rp_iv = random_bytes(16);
$rp_enc = openssl_encrypt($rp_renderProps, ALGO, $rp_pass, iv: $rp_iv);
$rp_url = "?" . http_build_query([
    "m" => "d",
    "r" => $rp_enc,
    "s" => $rp_salt,
    "v" => base64_encode($rp_iv),
]);

// render subject line as plaintext Twig template
$subject = (new Environment(new ArrayLoader([]), ["autoescape" => false]))
    ->createTemplate($templateProperties->subject)
    ->render($variables);

$variables["_meta"]["isBrowser"] = false;
$variables["_meta"]["subject"] = $subject;
$variables["_meta"]["viewInBrowserUrl"] = ($_ENV["PUBLIC_URL"] ?? "") . $rp_url;

if ($build_render_props) {
    result(data([
        "url" => $rp_url,
    ]));
} elseif ($output_rendered_html) {
    header("Content-Type: text/html; charset=UTF-8");

    $variables["_meta"]["isBrowser"] = true;
    $variables["_meta"]["viewInBrowserUrl"] = null;

    // render email template(s)
    $loader = new FilesystemLoader(TEMPLATE_DIRECTORY);

    $mailHtml = "";

    // HTML template
    if (is_file($htmlTemplateFile)) {
        $twigHtml = new Environment($loader);
        $mailHtml = $twigHtml->render("$templateName/body.html.twig", $variables);
    } else {
        http_response_code(404);
        exit;
    }

    // load inline media
    if (is_dir($mediaDir)) {
        foreach (scandir($mediaDir) as $ent) {
            if ($ent[0] === ".") continue;
            $path = "$mediaDir/$ent";
            if (!is_file($path)) continue;
            $mailHtml = str_replace("cid:$ent", "data:" . mime_content_type($path) . ";base64," . base64_encode(file_get_contents($path)), $mailHtml);
        }
    }

    echo $mailHtml;

    exit;
}

if (!($dsn = getenv("MAIL_DSN"))) {
    result(error("No transport DSN configured!"), 500);
}

$transport = Transport::fromDsn($dsn);
$mailer = new Mailer($transport);

$email = new Email();

$email->from(new Address(
    $templateProperties->sender->address,
    $templateProperties->sender->name,
));

$email->to(is_array($recipient) ? new Address(
    $recipient["address"],
    $recipient["name"],
) : $recipient);

$email->subject($subject);

// load inline media
if (is_dir($mediaDir)) {
    foreach (scandir($mediaDir) as $ent) {
        if ($ent[0] === ".") continue;
        $path = "$mediaDir/$ent";
        if (!is_file($path)) continue;
        $email->addPart((new DataPart(file_get_contents($path), $ent))->asInline());
    }
}

// add directly uploaded attachments
foreach ($_FILES as $fileKey => $file) {
    $email->attachFromPath($file["tmp_name"], $file["name"], $file["type"] ?? null);
}

// add attachments via URL
foreach ($urlAttachments as $index => $item) {
    if (is_array($item) && !isset($item["url"])) {
        result(error("Malformed field \"attachments[$index]\""), 400);
    }

    $url = is_array($item) ? $item["url"] : $item;

    if (!preg_match('/^https?:\/\//', $url)) {
        result(error("Malformed field \"attachments[$index]\""), 400);
    }

    // download attachment to a temporary file, so we can inspect the response headers
    $temp = tempnam(sys_get_temp_dir(), "att");
    $tempFiles []= $temp;
    copy($url, $temp);

    // crudely parse the response headers
    $headers = [];
    $headerCount = count($http_response_header);
    // we're skipping the first item as it's the HTTP version and status (which we're not interested in)
    for ($i = 1; $i < $headerCount; ++$i) {
        [$name, $value] = preg_split('/:\s*/', $http_response_header[$i], 2);
        $headers[strtolower($name)] = $value;
    }

    $responseContentType = $headers["content-type"] ?? null;
    $responseFilename = null;
    // try parsing the "Content-Disposition" header to extract the filename
    if (isset($headers["content-disposition"]) && str_contains($headers["content-disposition"], ";")) {
        $fields = preg_split('/;\s+/', $headers["content-disposition"]);
        $data = [];
        foreach ($fields as $field) {
            if (!str_contains($field, "=")) continue;
            [$key, $value] = explode('=', $field, 2);
            if (str_starts_with($value, '"') && str_ends_with($value, '"')) {
                $value = trim($value, '"');
            }
            $data[strtolower($key)] = $value;
        }
        $responseFilename = $data["filename"] ?? null;
    }

    if (is_array($item)) {
        $email->attachFromPath($temp, $item["filename"] ?? $responseFilename ?? basename($url), $item["mimeType"] ?? $responseContentType ?? null);
    } else {
        $email->attachFromPath($temp, $responseFilename ?? basename($item), $responseContentType);
    }
}

// render email template(s)
$loader = new FilesystemLoader(TEMPLATE_DIRECTORY);

// plaintext template
if (is_file($plaintextTemplateFile)) {
    $twigPlaintext = new Environment($loader, [
        "autoescape" => false,
    ]);
    $mailPlaintext = $twigPlaintext->render("$templateName/body.txt.twig", $variables);
    $email->text($mailPlaintext);
}

// HTML template
if (is_file($htmlTemplateFile)) {
    $twigHtml = new Environment($loader);
    $mailHtml = $twigHtml->render("$templateName/body.html.twig", $variables);
    $email->html($mailHtml);
}

try {
    $mailer->send($email);
    result(success());
} catch (TransportExceptionInterface $e) {
    result([
        "success" => false,
        "message" => $e->getMessage(),
        "error_type" => get_class($e),
    ], 500);
}
